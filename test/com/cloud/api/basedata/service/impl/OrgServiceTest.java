package com.cloud.api.basedata.service.impl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cloud.api.CloudApi;
import com.cloud.api.basedata.factory.BaseDataFactory;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.webapi.K3CloudApiClient;

class OrgServiceTest {
    // CloudApi 客户端
    static K3CloudApiClient client;

    /* Cloud访问地址 */ // http://oa.liminhot.com/K3Cloud/
    private static final String K3CloudURL = "http://111.30.26.49/K3Cloud/"; //

    /* 账套数据中心id */ // 5b8fc08f7bae55
    private static final String DBId = "5be9114fab8ddd";// 20180904143323

    /* 账户名 */
    private static final String userName = "Administrator";

    /* 密码 */ //111111
    private static final String passWord = "limin123";

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        client = CloudApi.getApiClient(K3CloudURL);
    }

    @Test
    void testSearch() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            String number = "3217";

            BaseDataService instance = BaseDataFactory.getInstance(BaseDataFactory.TYPE_ORG);
            System.out.println(instance.search(client, number));
        }
    }

}
