package com.cloud.api.basedata.service.impl;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cloud.api.CloudApi;
import com.cloud.webapi.K3CloudApiClient;

class BankAccountServiceTest {
    // CloudApi 客户端
    static K3CloudApiClient client;

    /* Cloud访问地址 */
    private static final String K3CloudURL = "http://111.30.26.49/K3Cloud/";

    /* 账套数据中心id */
    private static final String DBId = "5bc8534ffd54a8";

    /* 账户名 */
    private static final String userName = "demo";

    /* 密码 */
    private static final String passWord = "111111";

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        client = CloudApi.getApiClient(K3CloudURL);
    }

    @Test
    void testSave() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "1105");
            params.put("number", "1105");
            // 2221 天津市利民调料有限公司
            params.put("useOrgNum", "2221");

            BankAccountService service = new BankAccountService();
            String save = service.save(client, params);
            System.out.println(save);
        }
    }

    @Test
    void testUpdate() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "1105U2");
            params.put("number", "1105");
            params.put("billId", "137970");
            params.put("useOrgNum", "2221");

            BankAccountService service = new BankAccountService();
            String update = service.update(client, params);
            System.out.println(update);
        }
    }

    @Test
    void testSearch() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            String number = "9471105";

            BankAccountService service = new BankAccountService();
            String update = service.search(client, number);
            System.out.println(update);
        }
    }

    @Test
    void testForbidK3CloudApiClientString() {
        fail("Not yet implemented");
    }

    @Test
    void testsearchById() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            // String number = "9471105";
            int id = 108773;

            BankAccountService service = new BankAccountService();
            String update = service.searchById(client, id);
            System.out.println(update);
        }
    }

}
