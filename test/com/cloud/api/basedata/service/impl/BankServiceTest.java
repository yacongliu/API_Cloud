package com.cloud.api.basedata.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cloud.api.CloudApi;
import com.cloud.webapi.K3CloudApiClient;

class BankServiceTest {
    // CloudApi 客户端
    static K3CloudApiClient client;

    /* Cloud访问地址 */ // http://oa.liminhot.com/K3Cloud/
    private static final String K3CloudURL = "http://111.30.26.49/K3Cloud/"; //

    /* 账套数据中心id */ // 5b8fc08f7bae55
    private static final String DBId = "5bc8534ffd54a8";// 20180904143323

    /* 账户名 */
    private static final String userName = "demo";

    /* 密码 */
    private static final String passWord = "111111";

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        client = CloudApi.getApiClient(K3CloudURL);
    }

    @Test
    void testSave() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "0920测试接口");
            params.put("number", "2001.0016");
            // 2221 天津市利民调料有限公司
            params.put("useOrgNum", "2221");

            BankService service = new BankService();
            String save = service.save(client, params);
            System.out.println(save);
        }
    }

    @Test
    void testUpdate() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "0829测试接口4new2");
            params.put("number", "08290004");
            params.put("billId", "121298");
            params.put("useOrgNum", "2221");

            BankService service = new BankService();
            String update = service.update(client, params);
            System.out.println(update);
        }
    }

    @Test
    void testForbid() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            String number = "08240003";

            BankService service = new BankService();
            String forbid = service.forbid(client, number);
            System.out.println(forbid);
        }
    }

}
