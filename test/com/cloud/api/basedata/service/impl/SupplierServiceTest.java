package com.cloud.api.basedata.service.impl;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cloud.api.CloudApi;
import com.cloud.webapi.K3CloudApiClient;

class SupplierServiceTest {
    // CloudApi 客户端
    static K3CloudApiClient client;

    /* Cloud访问地址 */ // http://oa.liminhot.com/K3Cloud/
    private static final String K3CloudURL = "http://111.30.26.49/K3Cloud/"; //

    /* 账套数据中心id */ // 5b8fc08f7bae55
    private static final String DBId = "5be9114fab8ddd";// 20180904143323

    /* 账户名 */
    private static final String userName = "demo";

    /* 密码 */
    private static final String passWord = "111111";

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        client = CloudApi.getApiClient(K3CloudURL);
    }

    @Test
    void testSave() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "a3");
            params.put("number", "a3");
            // 2221 天津市利民调料有限公司
            params.put("useOrgNum", "3217");

            SupplierService service = new SupplierService();
            String saveRuturnInfo = service.save(client, params);
            System.out.println(saveRuturnInfo);
        }
    }

    @Test
    void testUpdate() {
        // 08230001  0823新测试接口1   old   billId 为单据保存后的单据id
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "0823新测试接口1Update");
            params.put("number", "08230001.01");
            params.put("billId", "120044");
            params.put("useOrgNum", "2221");

            SupplierService service = new SupplierService();
            System.out.println(service.update(client, params));
        }

    }

    @Test
    void testForbid() {
        fail("Not yet implemented");
    }

}
