package com.cloud.api.basedata.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cloud.api.CloudApi;
import com.cloud.api.basedata.factory.BaseDataFactory;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.JsonUtils;
import com.cloud.webapi.K3CloudApiClient;

class CustomerServiceTest {
    // CloudApi 客户端
    static K3CloudApiClient client;

    /* Cloud访问地址 */ // http://oa.liminhot.com/K3Cloud/
    private static final String K3CloudURL = "http://111.30.26.49/K3Cloud/"; //

    /* 账套数据中心id */ // 5b8fc08f7bae55
    private static final String DBId = "5be9114fab8ddd";// 20180904143323

    /* 账户名 */
    private static final String userName = "demo";

    /* 密码 */ // 111111
    private static final String passWord = "111111";

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        client = CloudApi.getApiClient(K3CloudURL);
    }

    @Test
    @Deprecated
    void testSave() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "0822新测试接口3");
            params.put("number", "08220003");
            // 2221 天津市利民调料有限公司
            params.put("useOrgNum", "2221");

            CustomerService service = new CustomerService();
            service.save(client, params);
        }
    }

    @Test
    @Deprecated
    void testUpdate() {
        // 119869 0822新测试接口2
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "0822新测试接口3new");
            params.put("number", "082200032");
            params.put("billId", "119873");
            params.put("useOrgNum", "2221");

            CustomerService service = new CustomerService();
            service.update(client, params);
        }
    }

    @Test
    void testForbid() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            String orgNum = "2221";
            String dataNumber = "3216";
            String number = orgNum + "!" + dataNumber;

            CustomerService service = new CustomerService();

            String res = service.delete(client, number);

            boolean flag = JsonUtils.parseJsonForDeleteResult(res);
            if (!flag) {
                // 删除失败 走 禁用接口
                res = service.forbid(client, number);
                System.out.println("删除失败，禁用资料！");
            }

            System.out.println(res);
        }
    }

    @Test
    void testSaveUseFactory() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "g3");
            params.put("number", "g3");
            // 2221 天津市利民调料有限公司
            params.put("useOrgNum", "2221");

            BaseDataService instance = BaseDataFactory.getInstance(BaseDataFactory.TYPE_CUSTOMER);
            System.out.println(instance.save(client, params));
        }
    }

    @Test
    void testUpdateUseFactory() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "1u");
            params.put("number", "2203");
            params.put("billId", "132966");
            params.put("useOrgNum", "3217");

            BaseDataService instance = BaseDataFactory.getInstance(BaseDataFactory.TYPE_CUSTOMER);
            System.out.println(instance.update(client, params));
        }
    }

    @Test
    void testSearchUseFactory() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {

            // 需要对编码进行组合 组织编码!客户编码
            // String number = "1";
            // String number = "2221!1";
            String number = "2203";
            String orgNumber = "3217";

            BaseDataService instance = BaseDataFactory.getInstance(BaseDataFactory.TYPE_CUSTOMER);
            if ("com.cloud.api.basedata.service.impl.CustomerService".equals(instance.getClass().getName())) {
                number = orgNumber + "!" + number;
            }
            System.out.println(instance.search(client, number));
        }
    }

}
