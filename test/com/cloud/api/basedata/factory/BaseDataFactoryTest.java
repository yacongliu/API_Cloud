package com.cloud.api.basedata.factory;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cloud.api.CloudApi;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.webapi.K3CloudApiClient;

class BaseDataFactoryTest {
    // CloudApi 客户端
    static K3CloudApiClient client;

    /* Cloud访问地址 */
    private static final String K3CloudURL = "http://oa.liminhot.com/K3Cloud/"; //http://oa.liminhot.com/K3Cloud/

    /* 账套数据中心id */
    private static final String DBId = "5b8fc08f7bae55";

    /* 账户名 */
    private static final String userName = "demo";

    /* 密码 */
    private static final String passWord = "111111";

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        client = CloudApi.getApiClient(K3CloudURL);
    }

    @Test
    void testGetInstance() {
        fail("Not yet implemented");
    }

    @Test
    void testTime() {
        long firstTime = System.currentTimeMillis();

        long lastTime = System.currentTimeMillis();

        System.out.println((lastTime - firstTime) / 1000);
    }

    @Test
    void testSave() {
        boolean login = CloudApi.login(client, DBId, userName, passWord);
        if (login) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "规模集成测试2");
            params.put("number", "0829000002");
            // params.put("billId", "121053");
            params.put("useOrgNum", "2221");
            
            Map<String, String> paramTaxrate = new HashMap<String, String>();
            params.put("name", "10%税率");
            params.put("number", "0829000002");
            // params.put("billId", "121053");
            params.put("useOrgNum", "2221");
            
            params.put("taxRate", "10");

            BaseDataService instance_c = BaseDataFactory.getInstance(BaseDataFactory.TYPE_CUSTOMER);
            System.out.println(instance_c.save(client, params));

            BaseDataService instance_s = BaseDataFactory.getInstance(BaseDataFactory.TYPE_SUPPLIER);
            System.out.println(instance_s.save(client, params));

            BaseDataService instance_b = BaseDataFactory.getInstance(BaseDataFactory.TYPE_BANK);
            System.out.println(instance_b.save(client, params));

            BaseDataService instance_ac = BaseDataFactory.getInstance(BaseDataFactory.TYPE_ACCOUNT);
            System.out.println(instance_ac.save(client, params));

            BaseDataService instance_bc = BaseDataFactory.getInstance(BaseDataFactory.TYPE_BANKACCOUNT);
            System.out.println(instance_bc.save(client, params));

            BaseDataService instance_d = BaseDataFactory.getInstance(BaseDataFactory.TYPE_DEPARTMENT);
            System.out.println(instance_d.save(client, params));

            BaseDataService instance_e = BaseDataFactory.getInstance(BaseDataFactory.TYPE_EMPLOYEE);
            System.out.println(instance_e.save(client, params));

            BaseDataService instance_t = BaseDataFactory.getInstance(BaseDataFactory.TYPE_TAXRATE);
            System.out.println(instance_t.save(client, paramTaxrate));
            
        }

    }

}
