package com.json.fastjson;

import org.junit.Test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cloud.api.utils.JsonUtils;

public class Json {

    @Test
    public void testJson() {
        String jsonString = "{\"Result\":{\"ResponseStatus\":{\"IsSuccess\":true,\"Errors\":[],\"SuccessEntitys\":[{\"Id\":127596,\"Number\":\"2221.06\",\"DIndex\":0}],\"SuccessMessages\":[],\"MsgCode\":0},\"Id\":127596,\"Number\":\"2221.06\",\"NeedReturnData\":[{\"FACCTID\":127596,\"FName\":\"待转销项税额\",\"FNumber\":\"2221.06\"}]}}";
        String details = null;
        String name = null;
        String number = null;
        String idStr = null;
        boolean success = false;

        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        JSONObject personObject = jsonObject.getJSONObject("Result");

        // id
        idStr = String.valueOf(personObject.getIntValue("Id"));

        JSONObject repStatus = personObject.getJSONObject("ResponseStatus");
        success = (boolean) repStatus.get("IsSuccess");
        JSONArray errorArr = repStatus.getJSONArray("Errors");
        if (errorArr.size() > 0) {
            details = (String) errorArr.get(0);
        }

        JSONArray returnDataArr = personObject.getJSONArray("NeedReturnData");
        if (returnDataArr.size() > 0) {
            JSONObject returnObj = JSONObject.parseObject(returnDataArr.get(0).toString());
            name = (String) returnObj.get("FName");
            number = (String) returnObj.get("FNumber");
        }

        System.out.println(details + " " + name + "   " + number + "   " + idStr + "   " + success);

    }

    @Test
    public void test() {
        System.out.println("12345\n1231313123");
    }

    @Test
    public void testContext() {
        String res = "{\"Result\":{\"ResponseStatus\":{\"ErrorCode\":500,\"IsSuccess\":false,\"Errors\":[{\"FieldName\":null,\"Message\":\"Context Is Null\",\"DIndex\":0}],\"SuccessEntitys\":[],\"SuccessMessages\":[],\"MsgCode\":1}}}";
    
        if(res.contains("Context Is Null")) {
            System.out.println("1111222");
        };
        
        if (JsonUtils.isContextNull(res)) {
//            login = CloudApi.login(client, dataId, user, pwd);

//            // 此次同步失败数据重新同步
//            resInfo = K3CloudMaster.actionBySyncType(client, instance, awaitSyncMap.get("syncType"),
//                    params);
            System.out.println("111");
        }
    }

}
