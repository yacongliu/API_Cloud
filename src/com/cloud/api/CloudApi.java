package com.cloud.api;

import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: CloudApi Description: Cloud API 获取连接客户端及登陆
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月22日
 */
public class CloudApi {
    /**
     * 语言 参考：中文2052，英文1033，繁体3076
     */
    private static final int LANG = 2052;

    /**
     * 
     * <p>
     * Title: Login
     * </p>
     * <p>
     * Description:
     * </p>
     * 登陆
     * 
     * @param client CloudApi连接客户端
     * @param DBId 数据中心ID
     * @param userName 用户名
     * @param passWord 密码
     * @return
     */
    public static boolean login(K3CloudApiClient client, String DBId, String userName, String passWord) {
        try {
            return client.login(DBId, userName, passWord, LANG);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("用户： " + userName + "  登陆失败！" + e);
        }

        return false;

    }

    /**
     * 
     * <p>
     * Title: getApiClient
     * </p>
     * <p>
     * Description:获取接口连接客户端
     * </p>
     * 
     * @param K3CloudURL Cloud接口访问地址. eg:http://xxxxxxx:xxxx/k3cloud/
     * @return K3CloudApiClient
     */
    public static K3CloudApiClient getApiClient(String k3CloudURL) {
        return new K3CloudApiClient(k3CloudURL);
    }

}
