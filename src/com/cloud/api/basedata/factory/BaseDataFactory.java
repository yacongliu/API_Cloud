package com.cloud.api.basedata.factory;

import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.basedata.service.impl.AccountService;
import com.cloud.api.basedata.service.impl.BankAccountService;
import com.cloud.api.basedata.service.impl.BankService;
import com.cloud.api.basedata.service.impl.CustomerService;
import com.cloud.api.basedata.service.impl.DepartmentService;
import com.cloud.api.basedata.service.impl.EmployeeService;
import com.cloud.api.basedata.service.impl.OrgService;
import com.cloud.api.basedata.service.impl.SupplierService;
import com.cloud.api.basedata.service.impl.TaxRateService;
import com.cloud.api.basedata.service.impl.ZXYFKService;

/**
 * 
 * Title: BaseDataFactory Description: 基础资料工厂 (设计模式：简单工厂模式)
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月28日
 */
public class BaseDataFactory {
    /**
     * 客户
     */
    public static final int TYPE_CUSTOMER = 1;

    /**
     * 供应商
     */
    public static final int TYPE_SUPPLIER = 2;

    /**
     * 部门
     */
    public static final int TYPE_DEPARTMENT = 3;

    /**
     * 税率
     */
    public static final int TYPE_TAXRATE = 4;

    /**
     * 银行(金融机构)
     */
    public static final int TYPE_BANK = 5;

    /**
     * 员工
     */
    public static final int TYPE_EMPLOYEE = 6;

    /**
     * 银行账号
     */
    public static final int TYPE_BANKACCOUNT = 7;

    /**
     * 科目
     */
    public static final int TYPE_ACCOUNT = 8;

    /**
     * 专项应付款
     */
    public static final int TYPE_ZXYFK = 9;
    
    /**
     * 组织机构
     */
    public static final int TYPE_ORG = 10;

    /**
     * 
     * <p>
     * Title: getInstance
     * </p>
     * <p>
     * Description: 获取基础资料实例
     * </p>
     * 
     * @param type
     * @return BaseDataService
     */
    public static BaseDataService getInstance(int type) {

        switch (type) {
        case TYPE_CUSTOMER:

            return new CustomerService();
        case TYPE_SUPPLIER:

            return new SupplierService();
        case TYPE_DEPARTMENT:

            return new DepartmentService();
        case TYPE_TAXRATE:

            return new TaxRateService();
        case TYPE_BANK:

            return new BankService();
        case TYPE_EMPLOYEE:

            return new EmployeeService();
        case TYPE_BANKACCOUNT:

            return new BankAccountService();
        case TYPE_ACCOUNT:

            return new AccountService();
        case TYPE_ZXYFK:

            return new ZXYFKService();
        case TYPE_ORG:

            return new OrgService();

        default:
            throw new IllegalArgumentException(" 非法的基础资料类型码！");
        }

    }
}
