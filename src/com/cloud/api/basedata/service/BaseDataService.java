package com.cloud.api.basedata.service;

/**
 * 
 * Title: BaseDataService
 * <p>
 * Description: 金蝶云星空 对应基础资料表单ID
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-9-12 & 下午02:39:20
 * @since V1.0
 */
public abstract class BaseDataService implements BaseAction {

    /**
     * 客户表单ID
     */
    protected static final String CUSTOMER_FORMID = "BD_Customer";

    /**
     * 供应商表单ID
     */
    protected static final String SUPPLIER_FORMID = "BD_Supplier";

    /**
     * 部门表单ID
     */
    protected static final String DEPARTMENT_FORMID = "BD_Department";

    /**
     * 税率表单ID
     */
    protected static final String TAXRATE_FORMID = "BD_TaxRate";

    /**
     * 银行表单ID
     */
    protected static final String BANK_FORMID = "BD_BANK";

    /**
     * 员工表单ID
     */
    protected static final String EMPLOYEE_FORMID = "BD_Empinfo";

    /**
     * 银行账号ID
     */
    protected static final String BANKACCOUNT_FORMID = "CN_BANKACNT";

    /**
     * 科目
     */
    protected static final String ACCOUNT_FORMID = "BD_Account";

    /**
     * 专项应付款
     */
    protected static final String ZXYFK_FORMID = "PAEZ_ZXYFKXM";
    
    /**
     * 组织结构
     */
    protected static final String ORG_FORMID = "ORG_Organizations";
    

}
