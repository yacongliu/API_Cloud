package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.OrgUtils;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: AccountService Description: 科目
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月28日
 */
public class AccountService extends BaseDataService implements BaseAction {

    /*
     * 分组类别 对应的是Cloud中的科目类别。经双方确认后，确认EAS同步的科目默认同步至Cloud中的表外科目类别 编码为 PRE15
     */
    private static final String GROUPID = "PRE15";

    /*
     * 对应的科目表 不指定的话会出现科目上下级关系转换出现问题 (编码带 . 时会出现错误：编码 FNumber 未将对象引用设置到对象实例)
     */
    private static final String ACCTTID = "PRE01";

    /**
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description: 新增科目
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(科目账号) number(科目编码) useOrgNum(科目编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String useOrgNum = params.get("useOrgNum");

            String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FACCTID\":0,\"FCreateOrgId\":{\"FNumber\":'"
                    + useOrgNum + "'},\"FACCTTBLID\":{\"FNumber\":'" + ACCTTID
                    + "'},\"FUseOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FFullName\":'" + name
                    + "',\"FNumber\":'" + number + "',\"FName\":'" + name + "',\"FGROUPID\":{\"FNumber\":'"
                    + GROUPID + "'}}}";

            try {
                return client.draft(ACCOUNT_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:科目  名称：" + name + "  编码：" + number + " 同步失败！" + e);
            }

        }

        return null;
    }

    /**
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description: 更新科目信息(编码、名称)
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(科目账号) number(科目编码) useOrgNum(科目编码) billId(目标单据ID)
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String billId = params.get("billId");
            // String useOrgNum = params.get("useOrgNum");

            int accountId = Integer.valueOf(billId);

            /*
             * String data =
             * "{\"Creator\":\"\",\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FACCTID\":'"
             * + accountId + "',\"FCreateOrgId\":{\"FNumber\":'" + useOrgNum +
             * "'},\"FUseOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FFullName\":'" + name +
             * "',\"FName\":'" + name + "'}}";
             */

            String data = "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FACCTID\":'"
                    + accountId + "',\"FName\":'" + name + "',\"FNumber\":'" + number + "'}}";

            try {

                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { ACCOUNT_FORMID, data }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:科目  名称：" + name + "  编码：" + number + " 修改失败！" + e);
            }

        }

        return null;
    }

    /**
     * 
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description: 禁用科目账号
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 禁用科目 组织编码!编码
     * @return 响应JSON串
     */
    @Override
    public String forbid(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";
            try {
                return client.excuteOperation(ACCOUNT_FORMID, "Forbid", data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 科目  编码： " + number + " 禁用失败！" + e);
            }
        }

        return null;

    }

    /**
     * <p>
     * Title: search
     * </p>
     * <p>
     * Description: 查询科目
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 科目编码 创建组织编码+客户编码 eg: 2221!CUS001 <br>
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#search(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String search(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String data = "{\"CreateOrgId\":\"0\",\"Number\":'" + number + "',\"Id\":\"\"}";

            try {
                return client.view(ACCOUNT_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 科目  编码： " + number + " 查询失败！" + e);
            }
        }

        return null;
    }

    /**
     * <p>
     * Title: delete
     * </p>
     * <p>
     * Description: 删除科目
     * </p>
     * 
     * @param client
     * @param number 组织编码!科目编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#delete(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String delete(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 科目编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }
            // {\"CreateOrgId\":\"0\",\"Numbers\":[],\"Ids\":\"\"}
            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.delete(ACCOUNT_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 科目  编码： " + number + " 删除失败！" + e);
            }
        }

        return null;
    }

}
