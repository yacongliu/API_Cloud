package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.OrgUtils;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: EmployeeService Description: 员工
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月24日
 */
public class EmployeeService extends BaseDataService implements BaseAction {

    /**
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description: 新增员工
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(员工姓名) number(员工编码) useOrgNum(使用组织编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#save(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String useOrgNum = params.get("useOrgNum");

            String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":0,\"FName\":'"
                    + name + "',\"FStaffNumber\":'" + number + "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum
                    + "'},\"FCreateOrgId\":{\"FNumber\":'" + useOrgNum + "'}}}";

            try {

                return client.draft(EMPLOYEE_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:员工  名称：" + name + "  编码：" + number + " 同步失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description: 更新员工信息(编码、姓名)
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(员工姓名) number(员工编码) useOrgNum(使用组织编码) billId(目标单据ID)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String billId = params.get("billId");
            // String useOrgNum = params.get("useOrgNum");

            int fid = Integer.valueOf(billId);

            /*
             * String data =
             * "{\"Creator\":\"\",\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":'"
             * + fid + "',\"FName\":'" + name + "',\"FStaffNumber\":'" + number +
             * "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FCreateOrgId\":{\"FNumber\":'" +
             * useOrgNum + "'}}}";
             */

            /*
             * String data =
             * "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":'"
             * + fid + "',\"FName\":'" + name + "',\"FStaffNumber\":'" + number + "'}}";
             */

            String data2 = "{\"NeedUpDateFields\":[\"FName\",\"FStaffNumber\"],\"NeedReturnFields\":[\"FName\",\"FStaffNumber\"],\"Model\":{\"FID\":'"
                    + fid + "',\"FName\":'" + name + "',\"FStaffNumber\":'" + number + "'}}";

            // String data3 =
            // "{\"NeedUpDateFields\":[],\"NeedReturnFields\":[],\"Model\":{\"FID\":137979,\"FName\":\"44\",\"FStaffNumber\":\"6676\"}}";

            try {
                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { EMPLOYEE_FORMID, data2 }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:员工  名称：" + name + "  编码：" + number + " 修改失败！" + e);
            }

        }

        return null;
    }

    /**
     * 
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description: 禁用员工
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 禁用员工编码
     * @return 响应JSON串
     */
    public String forbid(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";
            try {
                return client.excuteOperation(EMPLOYEE_FORMID, "Forbid", data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 员工  编码： " + number + " 禁用失败！" + e);
            }
        }

        return null;
    }

    /**
     * <p>
     * Title: search
     * </p>
     * <p>
     * Description: 查询员工
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 员工编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#search(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String search(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String data = "{\"CreateOrgId\":\"0\",\"Number\":'" + number + "',\"Id\":\"\"}";

            try {
                return client.view(EMPLOYEE_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 员工  编码： " + number + " 查询失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: delete
     * </p>
     * <p>
     * Description: 删除员工
     * </p>
     * 
     * @param client
     * @param number 组织编码!员工编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#delete(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String delete(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 员工编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }
            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.delete(EMPLOYEE_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 员工  编码： " + number + " 删除失败！" + e);
            }
        }
        return null;
    }

}
