package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.JsonUtils;
import com.cloud.api.utils.OrgUtils;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: BankAccountService Description: 银行账户
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月27日
 */
public class BankAccountService extends BaseDataService implements BaseAction {

    /**
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description: 新增银行账号
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(银行账号名称) number(银行账号编码) useOrgNum(使用组织编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#save(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String useOrgNum = params.get("useOrgNum");

            String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FBANKACNTID\":0,\"FCreateOrgId\":{\"FNumber\":'"
                    + useOrgNum + "'},\"FNumber\":'" + number + "',\"FName\":'" + name
                    + "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum
                    + "'},\"FBANKID\":{\"FNumber\":\"9905\"}}}";

            try {

                return client.draft(BANKACCOUNT_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:银行账号  名称：" + name + "  编码：" + number + " 同步失败！" + e);
            }
        }

        return null;
    }

    /**
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description: 更新银行账号(编码、名称)
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(银行账号) number(银行账号编码) useOrgNum(使用组织编码) billId(目标单据ID)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String billId = params.get("billId");
            // String useOrgNum = params.get("useOrgNum");
            /*
             * 税率接口要求 传输 开户银行 但修改的时候可以指定不修改开户银行，因此缺省设定为 9905
             */
            // String bankNum = "9905"; // 支付宝（中国）网络技术有限公司

            int bankAccountId = Integer.valueOf(billId);

            // 获取开户银行
            String searchRes = search(client, number);
            Map<String, String> map = JsonUtils.parseJsonForBankNumber(searchRes);
            String bankNumber = (map.get("bankNumber") == null) ? "" : map.get("bankNumber");

            if (StringUtils.isEmpty(bankNumber)) {
                bankNumber = "9905";
            }

            /*
             * bankAccountId = 136494; useOrgNum = "3215"; number = "23232324"; name = "123"; String
             * data3 =
             * "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[],\"Model\":{\"FBANKACNTID\":'"
             * + bankAccountId + "',\"FCreateOrgId\":{\"FNumber\":'" + useOrgNum +
             * "'},\"FNumber\":'"+number+"',\"FBANKID\":{\"FNumber\":\"9905\"},\"FName\":'"+name+
             * "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum + "'}}}";
             */

            String data = "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FBANKACNTID\":'"
                    + bankAccountId + "',\"FName\":'" + name + "',\"FNumber\":'" + number
                    + "',\"FBANKID\":{\"FNumber\":'" + bankNumber + "'}}";

            try {

                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { BANKACCOUNT_FORMID, data }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:银行账号  名称：" + name + "  编码：" + number + " 修改失败！" + e);
            }

        }
        return null;
    }

    /**
     * 
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description: 禁用银行账号
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 禁用银行账号编码
     * @return 响应JSON串
     */
    public String forbid(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.excuteOperation(BANKACCOUNT_FORMID, "Forbid", data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 银行账号  编码： " + number + " 禁用失败！" + e);
            }
        }

        return null;

    }

    /**
     * <p>
     * Title: search
     * </p>
     * <p>
     * Description: 查询银行账号
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 银行账号编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#search(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String search(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String data = "{\"CreateOrgId\":\"0\",\"Number\":'" + number + "',\"Id\":\"\"}";

            try {
                return client.view(BANKACCOUNT_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 银行账号  编码： " + number + " 查询失败！" + e);
            }
        }
        return null;
    }

    public String searchById(K3CloudApiClient client, int id) {
        String data = "{\"CreateOrgId\":\"0\",\"Number\":\"\",\"Id\":'" + id + "'}";

        try {

            return client.view(BANKACCOUNT_FORMID, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * <p>
     * Title: delete
     * </p>
     * <p>
     * Description: 删除银行账号
     * </p>
     * 
     * @param client
     * @param number 组织编码!银行账号编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#delete(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String delete(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 银行账号编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }
            // {\"CreateOrgId\":\"0\",\"Numbers\":[],\"Ids\":\"\"}
            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.delete(BANKACCOUNT_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 银行账号  编码： " + number + " 删除失败！" + e);
            }
        }
        return null;
    }

}
