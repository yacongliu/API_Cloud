package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.OrgUtils;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: ZXYFKService
 * <p>
 * Description:专项应付款
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-9-12 & 上午11:13:49
 * @since V1.0
 */
public class ZXYFKService extends BaseDataService implements BaseAction {

    /**
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description: 禁用专项应付款
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 禁用的专项应付款编码 组织编码!编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#forbid(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String forbid(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";
            try {
                return client.excuteOperation(ZXYFK_FORMID, "Forbid", data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 专项应付款  编码： " + number + " 禁用失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description: 新增专项应付款
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(专项应付款账号) number(专项应付款编码)
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#save(com.cloud.webapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {
        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            // 该自定义项目没有使用组织 为共享型基础资料
            // String useOrgNum = params.get("useOrgNum");

            String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":0,\"FNumber\":'"
                    + number + "',\"FName\":'" + name + "'}}";

            try {
                return client.draft(ZXYFK_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:专项应付款 名称：" + name + "  编码：" + number + " 同步失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description: 更新专项应付款信息(编码、名称)
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name( 专项应付款账号) number(专项应付款编码) billId(目标单据ID)
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#update(com.cloud.webapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {
        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String billId = params.get("billId");
            // String useOrgNum = params.get("useOrgNum");

            int zxyfkId = Integer.valueOf(billId);

            /*
             * String data =
             * "{\"Creator\":\"\",\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":'"
             * + zxyfkId + "',\"FNumber\":'" + number + "',\"FName\":'" + name + "'}}";
             */

            String data = "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":'"
                    + zxyfkId + "',\"FNumber\":'" + number + "',\"FName\":'" + name + "'}}";

            try {

                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { ZXYFK_FORMID, data }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:专项应付款  名称：" + name + "  编码：" + number + " 修改失败！" + e);
            }

        }
        return null;
    }

    /**
     * <p>
     * Title: search
     * </p>
     * <p>
     * Description: 查询专项应付款
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 专项应付款编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#search(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String search(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String data = "{\"CreateOrgId\":\"0\",\"Number\":'" + number + "',\"Id\":\"\"}";

            try {
                return client.view(ZXYFK_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 专项应付款  编码： " + number + " 查询失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: delete
     * </p>
     * <p>
     * Description: 删除专项应付款
     * </p>
     * 
     * @param client
     * @param number 组织编码!专项应付款编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#delete(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String delete(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 专项应付款编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }
            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.delete(ZXYFK_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 税率  编码： " + number + " 删除失败！" + e);
            }
        }
        return null;
    }

}
