package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.JsonUtils;
import com.cloud.api.utils.OrgUtils;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: TaxRateService Description: 税率
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月23日
 */
public class TaxRateService extends BaseDataService implements BaseAction {

    /**
     * 
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description: 新增税率
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params taxRate(税率值) name(税率名称) number(税率编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#save(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {
        if (params.size() > 0) {
            // 税率
            String taxRate = params.get("taxRate");
            // 编码
            String number = params.get("number");
            // 税率名称
            String name = params.get("name");

            // 税率值
            double trValue = Double.valueOf(taxRate);

            String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":0,\"FNumber\":'"
                    + number + "',\"FName\":'" + name + "',\"FTaxRate\":" + trValue
                    + ",\"FTaxatiionSystem\":{\"FNumber\":\"SSZD01_SYS\"},\"FTaxType\":{\"FNumber\":\"SZ01_SYS\"}}}";

            try {
                return client.draft(TAXRATE_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型：税率  编码：" + number + " 同步失败！" + e);
            }
        }

        return null;
    }

    /**
     * 
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description: 更新税率信息(编码 名称 税率值)
     * </p>
     * 
     * 
     * @param client CloudApi连接客户端
     * @param params taxRate(税率) number(税率编码) name(税率名称) billId(目标单据ID)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {
        if (params.size() > 0) {
            // 单据ID
            String billId = params.get("billId");

            // 税率
            String taxRate = null;
            // 编码
            String number = params.get("number");
            // 税率名称
            String name = params.get("name");
            if (name.contains("%")) {
                // 20%税率 以%作为分割 窃取税率值
                String[] split = name.split("%");
                taxRate = split[0];
            }
            int fid = Integer.valueOf(billId);

            // 税率值
            double trValue = (taxRate != null) ? Double.valueOf(taxRate) : 0.0;

            // 获取原值 税收制度:FTaxatiionSystem 税种:FTaxType
            String searchRes = search(client, number);
            Map<String, String> searchMap = JsonUtils.parseJsonForTaxRate(searchRes);
            String taxatiionSystem = (searchMap.get("taxatiionSystem") == null) ? ""
                    : searchMap.get("taxatiionSystem");
            String taxType = (searchMap.get("taxType") == null) ? "" : searchMap.get("taxType");
            String defaultCost = (searchMap.get("defaultCost") == null) ? "" : searchMap.get("defaultCost");

            if (StringUtils.isEmpty(taxatiionSystem)) {
                taxatiionSystem = "SSZD01_SYS";
            }

            if (StringUtils.isEmpty(taxType)) {
                taxType = "SZ01_SYS";
            }
            
            if (StringUtils.isEmpty(defaultCost)) {
                defaultCost = "FYXM02_SYS";
            }

            String data = "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FID\":'"
                    + fid + "',\"FNumber\":'" + number + "',\"FName\":'" + name + "',\"FTaxRate\":'" + trValue
                    + "',\"FTaxatiionSystem\":{\"FNumber\":'" + taxatiionSystem
                    + "'},\"FTaxType\":{\"FNumber\":'" + taxType + "'},\"FDefaultCost\":{\"FNumber\":'"+defaultCost+"'}}}";

            try {
                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { TAXRATE_FORMID, data }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型：税率  编码：" + number + " 修改失败！" + e);
            }
        }

        return null;
    }

    /**
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description: 禁用税率
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 禁用的税率编码
     * @return 响应JSON串
     */
    public String forbid(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";
            try {
                return client.excuteOperation(TAXRATE_FORMID, "Forbid", data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 税率  编码： " + number + " 禁用失败！" + e);
            }
        }

        return null;
    }

    /**
     * <p>
     * Title: search
     * </p>
     * <p>
     * Description: 查询税率
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 税率编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#search(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String search(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String data = "{\"CreateOrgId\":\"0\",\"Number\":'" + number + "',\"Id\":\"\"}";

            try {
                return client.view(TAXRATE_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 税率  编码： " + number + " 查询失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: delete
     * </p>
     * <p>
     * Description: 删除税率
     * </p>
     * 
     * @param client
     * @param number 组织编码!税率编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#delete(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String delete(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 税率编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }
            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.delete(TAXRATE_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 税率  编码： " + number + " 删除失败！" + e);
            }
        }
        return null;
    }

}
