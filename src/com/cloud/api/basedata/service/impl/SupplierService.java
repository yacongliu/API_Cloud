package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.api.utils.OrgUtils;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: SupplierService Description: 供应商
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年8月23日
 */
public class SupplierService extends BaseDataService implements BaseAction {

    /**
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description:新增供应商
     * </p>
     * 
     * 
     * @param client CloudApi连接客户端
     * @param params name(供应商名称) number(供应商编码) useOrgNum(Cloud使用组织编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#save(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {
        if (params.size() > 0) {
            // 名称
            String name = params.get("name");
            // 编码
            String number = params.get("number");
            // 使用组织
            String useOrgNum = params.get("useOrgNum");

            /*String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FSupplierId\":0,\"FCreateOrgId\":{\"FNumber\":'"
                    + useOrgNum + "'},\"FNumber\":'" + number + "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum
                    + "'},\"FName\":'" + name + "'}}";*/
            
            String data = "{\"Model\":{\"FSupplierId\":0,\"FCreateOrgId\":{\"FNumber\":'"
                    + useOrgNum + "'},\"FNumber\":'" + number + "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum
                    + "'},\"FName\":'" + name + "'}}";

            try {

                return client.draft(SUPPLIER_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:供应商  名称：" + name + "  编码：" + number + " 同步失败！" + e);
            }

        }
        return null;
    }

    /**
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description:
     * </p>
     * 更新供应商信息(名称、编码)
     * 
     * @param client CloudApi连接客户端
     * @param params name(供应商名称) number(供应商编码) billId(目标单据id) useOrgNum(Cloud使用组织编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {
        if (params.size() > 0) {
            // 名称
            String name = params.get("name");
            // 编码
            String number = params.get("number");
            // 单据ID
            String billId = params.get("billId");
            // 使用组织
            // String useOrgNum = params.get("useOrgNum");

            // 供应商单据ID(K3Cloud)
            int supplierId = Integer.valueOf(billId);

            /*
             * String data =
             * "{\"Creator\":\"\",\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FSupplierId\":'"
             * + supplierId + "',\"FCreateOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FNumber\":'" +
             * number + "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FName\":'" + name +
             * "'}}";
             */

            String data = "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FSupplierId\":'"
                    + supplierId + "',\"FNumber\":'" + number + "',\"FName\":'" + name + "'}}";

            try {
                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { SUPPLIER_FORMID, data }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:供应商  名称：" + name + "  编码：" + number + " 修改失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description:禁用供应商
     * </p>
     * 
     * 
     * @param client CloudApi连接客户端
     * @param number 禁用供应商编码
     * @return 响应JSON串
     * 
     */
    public String forbid(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 供应商编码
            String cusNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + cusNumber
                    + "']," + "\"Ids\":\"" + "\"}";
            try {
                return client.excuteOperation(SUPPLIER_FORMID, "Forbid", data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 供应商  编码： " + number + " 禁用失败！" + e);
            }
        }

        return null;
    }

    /**
     * <p>
     * Title: search
     * </p>
     * <p>
     * Description: 查询供应商
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param number 供应商编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#search(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String search(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 供应商编码
            String suplierNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }

            String data = "{\"CreateOrgId\":'" + createOrgId + "',\"Number\":'" + suplierNumber
                    + "',\"Id\":\"\"}";

            try {
                return client.view(SUPPLIER_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 供应商  编码： " + number + " 查询失败！" + e);
            }
        }
        return null;
    }

    /**
     * <p>
     * Title: delete
     * </p>
     * <p>
     * Description:删除供应商
     * </p>
     * 
     * @param client
     * @param number 组织编码!供应商编码
     * @return
     * @see com.cloud.api.basedata.service.BaseAction#delete(com.cloud.webapi.K3CloudApiClient,
     *      java.lang.String)
     */
    @Override
    public String delete(K3CloudApiClient client, String number) {

        if (StringUtils.isNotEmpty(number)) {
            String[] strArr = number.split("!");
            // 组织编码
            String orgNumber = strArr[0];
            // 供应商编码
            String dataNumber = strArr[1];

            // 根据组织编码 获取cloud中的组织内码
            int createOrgId = OrgUtils.getCloudOrgIdByOrgNumber(orgNumber);

            if (createOrgId == -1) {
                System.out.println("没有获取到组织内码！");
                return null;
            }
            // {\"CreateOrgId\":\"0\",\"Numbers\":[],\"Ids\":\"\"}
            String data = "{" + "\"CreateOrgId\":'" + createOrgId + "'," + "\"Numbers\":['" + dataNumber
                    + "']," + "\"Ids\":\"" + "\"}";

            try {
                return client.delete(SUPPLIER_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 供应商  编码： " + number + " 删除失败！" + e);
            }
        }
        return null;
    }

}
