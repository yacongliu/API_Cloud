package com.cloud.api.basedata.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cloud.api.basedata.service.BaseAction;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: OrgService Description: 组织机构
 * </p>
 * 
 * @author yacong_liu Email:2682505646@qq.com
 * @date 2018年10月23日
 */
public class OrgService extends BaseDataService implements BaseAction {

    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {
        return null;
    }

    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {
        return null;
    }

    @Override
    public String forbid(K3CloudApiClient client, String number) {
        return null;
    }

    @Override
    public String search(K3CloudApiClient client, String number) {
        if (StringUtils.isNotEmpty(number)) {
            String data = "{\"CreateOrgId\":\"0\",\"Number\":'" + number + "',\"Id\":\"\"}";

            try {
                System.out.println(client.view(ORG_FORMID, data));
                return client.view(ORG_FORMID, data);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型: 客户  编码： " + number + " 查询失败！" + e);
            }
        }
        return null;
    }

    @Override
    public String delete(K3CloudApiClient client, String number) {
        return null;
    }

}
