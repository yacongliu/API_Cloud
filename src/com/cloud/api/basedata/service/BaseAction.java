package com.cloud.api.basedata.service;

import java.util.Map;

import com.cloud.webapi.K3CloudApiClient;

public interface BaseAction {
    /**
     * 
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description: 新增
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params
     * @return
     */
    String save(K3CloudApiClient client, Map<String, String> params);

    /**
     * 
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description:修改名称或编码
     * </p>
     * 
     * @param client
     * @param params
     * @return
     */
    String update(K3CloudApiClient client, Map<String, String> params);

    /**
     * 
     * <p>
     * Title: forbid
     * </p>
     * <p>
     * Description:禁用
     * </p>
     * 
     * @param client
     * @param formId
     * @param number
     * @return
     */
    String forbid(K3CloudApiClient client,String number);
    
    /**
     * 
     * <p>Title: search</p>
     * <p>Description: 查询</p>
     * @param client
     * @param number
     * @return
     */
    String search(K3CloudApiClient client,String number);
    
    /**
     * 
     * <p>Title: delete</p>
     * <p>Description: 删除</p>
     * @param client
     * @param number
     * @return
     */
    String delete(K3CloudApiClient client,String number);

}
