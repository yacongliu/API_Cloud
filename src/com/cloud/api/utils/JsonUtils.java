package com.cloud.api.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * Title: JsonUtils
 * <p>
 * Description: JsonUtils 工具类
 * </p>
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-10-30 & 下午03:18:23
 * @since V1.0
 */
public class JsonUtils {

    /**
     * 
     * <p>
     * Title: parseJsonForId
     * </p>
     * <p>
     * Description: 解析Cloud中的 查询返回 Json 获取 Id
     * </p>
     * 
     * @return
     */
    public static int parseJsonForId(String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONObject object = jsonObject.getJSONObject("Result");
        JSONObject resObj = object.getJSONObject("Result");
        return resObj.getIntValue("Id");

    }

    /**
     * 
     * <p>
     * Title: parseJsonForDeleteResult
     * </p>
     * <p>
     * Description: 解析json 获取是否删除成功
     * </p>
     * 
     * @param json
     * @return
     */
    public static boolean parseJsonForDeleteResult(String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONObject object = jsonObject.getJSONObject("Result");
        JSONObject resObj = object.getJSONObject("ResponseStatus");

        if (JSONObject.toJSONString(resObj).contains("IsSuccess")) {
            return resObj.getBooleanValue("IsSuccess");
        }
        return true;

    }

    /**
     * 
     * <p>
     * Title: parseJsonForTaxRate
     * </p>
     * <p>
     * Description: 解析查询税率Json 获取修改接口需要的内容
     * </p>
     * 
     * @param json
     * @return
     */
    public static Map<String, String> parseJsonForTaxRate(String json) {
        Map<String, String> map = new HashMap<String, String>(2);

        String taxatiionSystem = null;
        String taxType = null;
        String defaultCost = null;

        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONObject object = jsonObject.getJSONObject("Result");
        JSONObject resObj = object.getJSONObject("Result");
        // 税收制度
        int taxatiionSystem_Id = resObj.getIntValue("TaxatiionSystem_Id");
        // 税种
        int taxTypeId = resObj.getIntValue("TaxType_Id");
        // 默认费用项目
        int defaultCost_Id = resObj.getIntValue("DefaultCost_Id");

        if (taxatiionSystem_Id != 0) {
            JSONObject taxSystemObj = resObj.getJSONObject("TaxatiionSystem");
            if (null != taxSystemObj) {
                taxatiionSystem = taxSystemObj.getString("Number");
            }
        } else {
            taxatiionSystem = "";
        }

        if (taxTypeId != 0) {
            JSONObject taxTypeObj = resObj.getJSONObject("TaxType");
            if (null != taxTypeObj) {
                taxType = taxTypeObj.getString("Number");
            }
        } else {
            taxType = "";
        }

        if (defaultCost_Id != 0) {
            JSONObject dCostObj = resObj.getJSONObject("DefaultCost");
            if (null != dCostObj) {
                defaultCost = dCostObj.getString("Number");
            }
        } else {
            defaultCost = "";
        }

        map.put("taxatiionSystem", taxatiionSystem);
        map.put("taxType", taxType);
        map.put("defaultCost", defaultCost);

        return map;

    }

    /**
     * 
     * <p>
     * Title: parseJsonForTaxRate
     * </p>
     * <p>
     * Description: 解析查询银行账号Json 获取修改接口需要的内容
     * </p>
     * 
     * @param json
     * @return
     */
    public static Map<String, String> parseJsonForBankNumber(String json) {
        Map<String, String> map = new HashMap<String, String>(2);

        String bankNumber = null;

        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONObject object = jsonObject.getJSONObject("Result");
        JSONObject resObj = object.getJSONObject("Result");

        // 为0时，没有开户银行信息
        int bankId = resObj.getIntValue("BANKID_Id");
        if (bankId == 0) {
            map.put("bankNumber", "");
        } else {

            JSONObject bankObj = resObj.getJSONObject("BANKID");
            if (null != bankObj) {
                bankNumber = bankObj.getString("Number");
            }

            map.put("bankNumber", bankNumber);
        }
        return map;

    }

    /**
     * 
     * <p>
     * Title: parseJsonForSearchBaseDataID
     * </p>
     * <p>
     * Description: 查询基础资料内码 是否成功
     * </p>
     * 
     * @param json
     * @return
     */
    public static boolean parseJsonForSearchBaseDataID(String json) {
        return parseJsonForDeleteResult(json);
    }

    

    /**
     * 
     * <p>
     * Title: isContextNull
     * </p>
     * <p>
     * Description: 与cloud交互，判断Context是否为null,会话超时
     * </p>
     * 
     * @param json
     * @return
     */
    public static boolean isContextNull(String json) {

        if (StringUtils.isEmpty(json)) {
            return false;
        }

        JSONObject jsonObject = JSONObject.parseObject(json);
        JSONObject resObject = jsonObject.getJSONObject("Result");
        JSONObject responseStatus = resObject.getJSONObject("ResponseStatus");
        if (JSONObject.toJSONString(responseStatus).contains("IsSuccess")) {

            Boolean flag = responseStatus.getBoolean("IsSuccess");
            if (!flag) {
                JSONObject errObj = responseStatus.getJSONArray("Errors").getJSONObject(0);
                String msg = errObj.getString("Message");
                if ("Context Is Null".equalsIgnoreCase(msg)) {
                    return true;
                }
            }
        }

        return false;

    }
}
