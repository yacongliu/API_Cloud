package com.cloud.api.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Properties;

import com.cloud.api.CloudApi;
import com.cloud.api.basedata.factory.BaseDataFactory;
import com.cloud.api.basedata.service.BaseDataService;
import com.cloud.webapi.K3CloudApiClient;

/**
 * 
 * Title: OrgUtils
 * <p>
 * Description:Cloud 组织机构
 * </p>
 * 
 * @author yacong_liu Email:yacong_liu@kingdee.com
 * @date 2018-10-23 & 下午01:44:15
 * @since V1.0
 */
public class OrgUtils {

    /* Cloud访问地址 */
    private static String K3CloudURL = null;

    /* 账套数据中心id */
    private static String DBId = null;

    /* 账户名 */
    private static String userName = null;

    /* 密码 */
    private static String passWord = null;

    static {
        Properties properties = new Properties();
        // 读取利民Cloud 管理员账户配置信息
        try {
            String path = OrgUtils.class.getResource("/").getPath();
            InputStream is = new BufferedInputStream(new FileInputStream(path + "EIP.properties"));
            properties.load(is);

            // 接口访问地址
            K3CloudURL = properties.getProperty("LIMIN_K3CloudURL");
            // 账套ID
            DBId = properties.getProperty("LIMIN_DBID");
            userName = properties.getProperty("LIMIN_USERNAME");
            passWord = properties.getProperty("LIMIN_PASSWORD");
            
            System.out.println("Cloud管理员配置：" + "地址:" + K3CloudURL + "账套ID:" + DBId + "用户：" + userName + "密码："
                    + URLEncoder.encode(passWord, "UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("没有找到管理员配置文件（EIP.properties）！");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("加载配置文件出错！");
        }
    }

    /**
     * 
     * <p>
     * Title: getCloudOrgIdByOrgNumber
     * </p>
     * <p>
     * Description: 根据组织编码获取其在Cloud中的组织内码
     * </p>
     * 
     * @return
     * @throws UnsupportedEncodingException
     */
    public static int getCloudOrgIdByOrgNumber(String orgNumebr) {
        // getConfigForAdministrator();

        K3CloudApiClient apiClient = CloudApi.getApiClient(K3CloudURL);
        if (CloudApi.login(apiClient, DBId, userName, passWord)) {
            BaseDataService instance = BaseDataFactory.getInstance(BaseDataFactory.TYPE_ORG);
            String orgRes = instance.search(apiClient, orgNumebr);
            return JsonUtils.parseJsonForId(orgRes);
        }
        return -1;

    }

    /**
     * 
     * <p>
     * Title: getConfigForAdministrator
     * </p>
     * <p>
     * Description: 获取Cloud 管理员配置信息
     * </p>
     */
    @Deprecated
    private static void getConfigForAdministrator() {
        Properties properties = new Properties();
        // 读取利民Cloud 管理员账户配置信息
        try {
            String path = OrgUtils.class.getResource("/").getPath();
            InputStream is = new BufferedInputStream(new FileInputStream(path + "EIP.properties"));
            properties.load(is);

            // 接口访问地址
            K3CloudURL = properties.getProperty("LIMIN_K3CloudURL");
            // 账套ID
            DBId = properties.getProperty("LIMIN_DBID");
            userName = properties.getProperty("LIMIN_USERNAME");
            passWord = properties.getProperty("LIMIN_PASSWORD");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("没有找到管理员配置文件（EIP.properties）！");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("加载配置文件出错！");
        }
    }
}
