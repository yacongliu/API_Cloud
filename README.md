# 简介

调用金蝶云星空Cloud示例代码

同步基础资料至金蝶云星空

涉及的基础资料如下：

+ 科目
+ 银行账号
+ 银行
+ 客户
+ 部门
+ 员工
+ 组织机构
+ 供应商
+ 税率
+ 专项应付款（自定义基础资料）

# 接口说明

其中一个很重要的”坑“！Cloud中的保存和修改接口共用的一个接口。区别就在于是否给定单据id !!! 

如果传输的JSON字符串中id为 0 则表示新增。给定 id 内容 则视为 修改。

例如，以客户接口为例：

- 客户新增数据格式

```
/**
     * <p>
     * Title: save
     * </p>
     * <p>
     * Description:新增客户
     * </p>
     * 
     * 
     * @param client CloudApi连接客户端
     * @param params name(客户名称) number(客户编码) useOrgNum(Cloud使用组织编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#save(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String save(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            // 客户名称
            String name = params.get("name");
            String number = params.get("number");
            String useOrgNum = params.get("useOrgNum");

            // 0 表示新增
            int cusId = 0;
            String data = "{\"Creator\":\"\",\"NeedUpDateFields\":[],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FCUSTID\":'"
                    + cusId + "',\"FCreateOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FNumber\":'" + number
                    + "',\"FUseOrgId\":{\"FNumber\":'" + useOrgNum + "'},\"FName\":'" + name + "'}}";

            try {
                return client.draft(CUSTOMER_FORMID, data);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:客户  名称：" + name + "  编码：" + number + " 同步失败！" + e);
            }
        }

        return null;
    }
```

- 客户修改数据格式

```
/**
     * <p>
     * Title: update
     * </p>
     * <p>
     * Description:更新客户信息(编码、名称)
     * </p>
     * 
     * @param client CloudApi连接客户端
     * @param params name(客户名称) number(客户编码) billId(目标单据id) useOrgNum(Cloud使用组织编码)
     * @return 响应JSON串
     * @see com.cloud.api.basedata.service.BaseAction#update(com.lifei.k3cloudwebapi.K3CloudApiClient,
     *      java.util.Map)
     */
    @Override
    public String update(K3CloudApiClient client, Map<String, String> params) {

        if (params.size() > 0) {
            String name = params.get("name");
            String number = params.get("number");
            String billId = params.get("billId");
            // String useOrgNum = params.get("useOrgNum");

            int cusId = Integer.valueOf(billId);

            String data = "{\"NeedUpDateFields\":[\"FName\",\"FNumber\"],\"NeedReturnFields\":[\"FName\",\"FNumber\"],\"Model\":{\"FCUSTID\":'"
                    + cusId + "',\"FNumber\":'" + number + "',\"FName\":'" + name + "'}}";
            try {
                return client.execute("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save",
                        new Object[] { CUSTOMER_FORMID, data }, String.class);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("基础资料类型:客户  名称：" + name + "  编码：" + number + " 修改失败！" + e);
            }

        }

        return null;
    }
```





